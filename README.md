# Building

Set `$dir` to the path to the `rkt` data directory then start the build
pod:

```shell
sudo rkt \
    --debug=true \
    --dir="${dir}" \
    --insecure-options=image \
    run \
    docker://lambci/lambda:build-python3.6 \
    --net=host --dns 8.8.8.8 \
    --volume lambda,kind=host,source="$PWD",readOnly=false \
    --mount volume=lambda,target=/var/task \
    --uuid-file-save=./python-lambda-build \
    --interactive \
    --exec /bin/bash
```

Then install `scrapy` and its dependencies to `./vendor`:

```sh
python3.6 -m pip install yolk3k &&
yolk --debug -F Twisted -T source &&
bzip2 -v -d Twisted*.tar.bz2 &&
python -m pip install -t vendor ./Twisted-*.tar scrapy &&
rm -f ./Twisted-*.tar
```

The above uses a workaround because Twisted only provides a `.tar.bz2` and the
Python 3.6 `bz2` module in the container is broken.

Clean up the exited pod:

```sh
sudo rkt --dir="${dir}" rm --uuid-file=./python-lambda-build &&
rm ./python-lambda-build
```

Then test that the function runs:

```sh
sudo rkt \
    --debug=true \
    --dir=$dir \
    --insecure-options=image \
    run \
    docker://lambci/lambda:python3.6 \
    --group=495 \
    --net=host --dns 8.8.8.8 \
    --volume lambda,kind=host,source="$PWD",readOnly=true \
    --mount volume=lambda,target=/var/task \
    --uuid-file-save=./python-lambda &&
sudo rkt --dir="${dir}" rm --uuid-file=./python-lambda &&
rm ./python-lambda
```

Zip the result:

```sh
python -m zipfile -c "${PWD##*/}.zip" vendor/ lambda_function.py
```
