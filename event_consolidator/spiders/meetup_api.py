# -*- coding: utf-8 -*-
import scrapy
import json
from scrapy.utils.project import get_project_settings

SETTINGS = get_project_settings()
GROUP_EVENT_ENDPOINT = 'https://api.meetup.com/{group_name}' \
    '/events?&sign=true&key={api_key}'

class MeetupApiSpider(scrapy.Spider):
    name = 'meetup_api'
    allowed_domains = ['api.meetup.com']

    def start_requests(self):
        api_key = SETTINGS.get('MEETUP_API_KEY')
        for group_name in SETTINGS.get('MEETUP_GROUP_NAMES'):
            uri = GROUP_EVENT_ENDPOINT.format(
                group_name=group_name,
                api_key=api_key
            )
            yield scrapy.Request(uri)

    def parse(self, response):
        event = json.loads(
            response.body_as_unicode()
        )
